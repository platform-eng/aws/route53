module "route53_records" {
  source              = "terraform-aws-modules/route53/aws//modules/records"
  version             = "2.10.2"
  for_each            = var.route53_records
  zone_id             = try(each.value.zone_id, var.zone_id)
  zone_name           = each.key
  private_zone        = try(each.value.private_zone, var.private_zone)
  records             = try(each.value.records, var.records)
  records_jsonencoded = try(each.value.records_jsonencoded, var.records_jsonencoded)
  depends_on          = [module.route53_zones]
}
module "route53_zones" {
  source  = "terraform-aws-modules/route53/aws//modules/zones"
  version = "2.10.2"
  zones   = try(var.route53_zones.zones, var.zones)
  tags    = try(var.route53_zones.tags, var.tags)
}
