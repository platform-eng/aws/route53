
variable "route53_records" {
  type    = any
  default = {}
}
variable "route53_zones" {
  type    = any
  default = {}
}
variable "common_config" {
  type = any
}
variable "zone_id" {
  description = "ID of DNS zone"
  type        = string
  default     = null
}
variable "zone_name" {
  description = "Name of DNS zone"
  type        = string
  default     = null
}

variable "private_zone" {
  description = "Whether Route53 zone is private or public"
  type        = bool
  default     = false
}

variable "records" {
  description = "List of objects of DNS records"
  type        = any
  default     = []
}

variable "records_jsonencoded" {
  description = "List of map of DNS records (stored as jsonencoded string, for terragrunt)"
  type        = string
  default     = null
}
variable "zones" {
  description = "Map of Route53 zone parameters"
  type        = any
  default     = {}
}
variable "tags" {
  description = "Tags added to all zones. Will take precedence over tags from the 'zones' variable"
  type        = map(any)
  default     = {}
}