# AWS Route53 using YAML

- To get started using terraform provisioners please follow the steps in [`documentation`](https://gitlab.com/platform-eng/documentation/-/blob/main/getting-started-with-provisioners.md)

- For provisioning Route53, use `route53_zones` and `route53_records` element. Under this element specify zones and zone_name respectively. 

  ```yaml
  backend_config:
    backend: s3
    bucket: <bucket_name>
    key: <key_name>
    region: us-west-1
    dynamodb_table: <table_name>

  common_config:
    allowed_account_ids:
      - <account_id_1>
      - <account_id_2>
    region: <region>
    default_tags:
      <key1>: <value1>
      <key2>: <value2>
 
  route53_zones:
    zones :
      #if you do not want to specify comments or tags inside this zone use {} to keep it empty else it will give an error.
      "myapp.com" : {}
    #comman tags applied to all zones which you mention
    tags :
      ManagedBy : "Terraform"
        
  route53_records:
    #zone name and inside it specify its records which all you want to create
    myapp.com:
      records : 
        - name : ""
          type : "A"
          ttl  : 3600
          records : 
            - "10.10.10.10"


        
  ```
  If you want only the records or only the zones to be created you can specify only that particular field as well.
  Inside `route53_zones` if you want multiple zones to be created then below `myapp.com`you can specify other zone_name and any configuration if you want.
  Inside `route53_records` you can specify multiple records under one zone i.e `myapp.com`.Even you can give one more zone name under `myapp.com` and specify its different records.

  *Note :  ` backend_config` and `common_config` are required while provisioning resources. Resources will not be provisioned untill there is ` backend_config` and `common_config` block in YAML file.*<br>

- **For templates of YAML configuration file, please go to template folder in repository.**

## Supported inputs by `route53_zones` element which is used in `YAML` : 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_tags"></a> [tags](#input\_tags) | Tags added to all zones. Will take precedence over tags from the 'zones' variable | `map(any)` | `{}` | no |
| <a name="input_zones"></a> [zones](#input\_zones) | Map of Route53 zone parameters | `any` | `{}` | no |

## Supported inputs by `route53_records` element which is used in `YAML` : 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_private_zone"></a> [private\_zone](#input\_private\_zone) | Whether Route53 zone is private or public | `bool` | `false` | no |
| <a name="input_records"></a> [records](#input\_records) | List of objects of DNS records | `any` | `[]` | no |
| <a name="input_records_jsonencoded"></a> [records\_jsonencoded](#input\_records\_jsonencoded) | List of map of DNS records (stored as jsonencoded string, for terragrunt) | `string` | `null` | no |
| <a name="input_zone_id"></a> [zone\_id](#input\_zone\_id) | ID of DNS zone | `string` | `null` | no |
| <a name="input_zone_name"></a> [zone\_name](#input\_zone\_name) | Name of DNS zone | `string` | `null` | no |


<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
